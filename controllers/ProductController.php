<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/28/2018
 * Time: 10:40
 */

namespace app\controllers;


use yii\web\Controller;
use app\models\Product;

class ProductController extends Controller
{
    public function actionIndex()
    {
        $model = Product::find()->all();
        return $this->render('index',[
            'model' => $model,

        ]);
    }

}