<?php

use yii\db\Migration;

/**
 * Class m180514_134706_products_tables
 */
class m180514_134706_products_tables extends Migration
{
    protected $tn_product = '{{%product}}';
    protected $tn_category = '{{%category}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->tn_category, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);

        $this->createTable($this->tn_product, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'text' => $this->text(),
            'price' => $this->float(),
            'keywords' => $this->string(),
            'img' => $this->string(),
            'user_id' => $this->integer(),


        ]);

        $this->addForeignKey('product_category_id',
            $this->tn_product,
            'category_id',
            $this->tn_category,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('product_category_id', $this->tn_product);
        $this->dropTable($this->tn_product);
        $this->dropTable($this->tn_category);
    }
}